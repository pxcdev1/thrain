package sse

import "time"

type Event struct {
	ID        []byte
	Data      []byte
	Event     []byte
	Retry     []byte
	timestamp time.Time
}
