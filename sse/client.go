package sse

import (
	"context"
	"fmt"
	"net/http"
	//backoff "gopkg.in/cenkalti/backoff.v1"
)

type Client struct {
	URL        string
	Connection *http.Client
	Headers    map[string]string
	EventID    string
}

func NewClient(url string) *Client {
	c := &Client{
		URL:        url,
		Connection: &http.Client{},
		Headers:    make(map[string]string),
	}

	return c
}

func (c *Client) Subscribe(topic string, handler func(msg *Event)) error {
	ctx := context.Background()
	// No retry at first
	resp, err := c.request(ctx, topic)
	// We have no connection at all
	if err != nil {
		return err
	}
	// We have a connection, but with an error
	if resp.StatusCode != 200 {
		resp.Body.Close()
		return fmt.Errorf("could not connect to stream: %s", http.StatusText(resp.StatusCode))
	}
	// We have a connection
	defer resp.Body.Close()

	return nil
}

func (c *Client) request(ctx context.Context, topic string) (*http.Response, error) {
	req, err := http.NewRequestWithContext(ctx, "GET", c.URL, nil)
	if err != nil {
		return nil, err
	}

	if topic != "" {
		query := req.URL.Query()
		query.Add("topic", topic)
		req.URL.RawQuery = query.Encode()
	}

	req.Header.Set("Cache-Control", "no-cache")
	req.Header.Set("Accept", "text/event-stream")
	req.Header.Set("Connection", "keep-alive")

	if c.EventID != "" {
		req.Header.Set("Last-Event_ID", c.EventID)
	}

	// Add user specific headers
	for k, v := range c.Headers {
		req.Header.Set(k, v)
	}

	return c.Connection.Do(req)
}
